<?php

namespace BBCode\Exception;

/**
 * an exception class to report invalid tag nesting
 *
 * @package BBCode\Exception
 */
class InvalidParentException extends TagRenderException {

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', 'tag [%s] is not allowed in tag [%s]');
        parent::__construct($message, $code, $previous);
    }

}