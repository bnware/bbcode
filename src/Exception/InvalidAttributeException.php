<?php

namespace BBCode\Exception;

/**
 * an exception class to report any attribute error, e.g. invalid value
 *
 * @package BBCode\Exception
 */
class InvalidAttributeException extends TagRenderException {

    protected $_messageTemplate = "Invalid attribute '%s' in tag [%s]: %s";

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', "Invalid attribute '%s' in tag [%s]: %s");
        parent::__construct($message, $code, $previous);
    }

}