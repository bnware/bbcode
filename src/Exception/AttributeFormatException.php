<?php

namespace BBCode\Exception;

/**
 * an exception class to report invalid attribute syntax
 *
 * @package BBCode\Exception
 */
class AttributeFormatException extends BBCodeException {

    public function __construct ($message, $position, $bbCode) {
        $this->_message = __d('BBCode', "Invalid attribute definition '%s' in tag [%s]");
        parent::__construct($message, $position, $bbCode);
    }

}