<?php

namespace BBCode\Exception;

/**
 * an exception class to report missing required attributes
 *
 * @package BBCode\Exception
 */
class MissingAttributeException extends TagRenderException {

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', "missing attribute '%s' in tag [%s]");
        if (is_array($message[0])) {
            $this->_messageTemplate = __d('BBCode', 'tag [%2$s] must have at least one of these attributes: %1$s');
            $message[0] = implode(', ', $message[0]);
        }
        parent::__construct($message, $code, $previous);
    }

}