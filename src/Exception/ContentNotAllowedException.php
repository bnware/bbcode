<?php

namespace BBCode\Exception;

/**
 * an exception class to report content in standalone tags
 *
 * @package BBCode\Exception
 */
class ContentNotAllowedException extends TagRenderException {

    public function __construct ($message, $code = 500, $previous = null) {
        $this->_messageTemplate = __d('BBCode', 'standalone tag [%s] is not allowed to have content');
        parent::__construct($message, $code, $previous);
    }

}