<?php

namespace BBCode\Lib\Parser;

/**
 * the internal status stack implementation used to stack status objects
 *
 * @package BBCode\Lib\Parser
 */
class StatusStack {

    /**
     * @var Status[]
     */
    private $_stack = [];

    /**
     * pushes the given status object onto the stack
     *
     * @param Status $status the status to store
     */
    public function push (Status &$status) {
        $this->_stack[] = $status;
    }

    /**
     * returns the next status object from the stack and removes it
     *
     * @return Status the next status object
     */
    public function pop () {
        return array_pop($this->_stack);
    }

    /**
     * returns the number of status objects stored in the stack
     *
     * @return int the number of status objects
     */
    public function count () {
        return count($this->_stack);
    }

    /**
     * returns the requested status object without removing it from the stack or a new empty status object if invalid index
     *
     * @param int $i the index to return
     * @return Status
     */
    public function get ($i = 1) {
        return @$this->_stack[$i] ?: new Status();
    }

    function __toString () {
        return '[' . $this->count() . ']';
    }

}