<?php

namespace BBCode\Lib\Parser;

/**
 * the internal status class of the BBCode parser
 *
 * @package BBCode\Lib\Parser
 */
class Status {

    /**
     * @var bool whether the next char is escaped or not
     */
    public $escape = false;

    /**
     * @var bool whether the cursor is inside a string or not
     */
    public $string = false;

    /**
     * @var bool whether the cursor is inside tag brackets or not
     */
    public $tag= false;

    /**
     * @var int the current tag type (one of the TagType constants)
     */
    public $type= TagType::StartTag;

    /**
     * @var string the already rendered HTML
     */
    public $html= '';

    /**
     * @var string the name of the current tag
     */
    public $tagName= '';

    /**
     * @var array the already parsed attributes of the current tag
     */
    public $attributes= [];

    /**
     * toggles the escape state
     */
    public function toggleEscape () {
        $this->escape = !$this->escape;
    }

    /**
     * toggles the string state
     */
    public function toggleString () {
        $this->string = !$this->string;
    }

    /**
     * @return bool whether the current tag is a start tag or not
     */
    public function isStartTag () {
        return $this->type == TagType::StartTag;
    }

    /**
     * @return bool whether the current tag is an end tag or not
     */
    public function isEndTag () {
        return $this->type == TagType::EndTag;
    }

    /**
     * @return bool true, if no tag name is currently determined, false otherwise
     */
    public function isNoTagName() {
        return empty($this->tagName);
    }

    function __toString () {
        $string = $this->html . '[';
        if ($this->isEndTag()) {
            $string .= '/';
        }
        $string .= $this->tagName;
        foreach ($this->attributes as $name => $value) {
            $string .= sprintf(' %s="%s"', $name, $value);
        }
        if ($this->type == TagType::StandaloneTag) {
            $string .= '/';
        }
        return $string . ']';
    }

}