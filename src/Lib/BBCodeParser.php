<?php

namespace BBCode\Lib;

use BadMethodCallException;
use BBCode\Exception\AttributeFormatException;
use BBCode\Exception\ContentNotAllowedException;
use BBCode\Exception\InvalidTagNameException;
use BBCode\Exception\TagRenderException;
use BBCode\Exception\UnexpectedStringException;

use BBCode\Exception\UnknownTagException;
use BBCode\Lib\Parser\Status;
use BBCode\Lib\Parser\StatusStack;
use BBCode\Lib\Parser\TagType;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Event\EventManager;
use Cake\View\CellTrait;
use Cake\View\Exception\MissingCellException;
use Cake\View\Exception\MissingCellViewException;
use Cake\View\Helper;
use Exception;

/**
 * the library class implementing the parser
 *
 * @package BBCode\Lib
 */
class BBCodeParser {

    use CellTrait {cell as protected;}

    private $_strict;

    /**
     * constructs a new parser instance.
     *
     * in strict mode, all errors cause an exception to be thrown, otherwise the parser will try to generate as much as possible output
     *
     * @param bool $strict whether to run in strict mode or not
     */
    public function __construct ($strict = false) {
        $this->_strict = $strict;
    }

    /**
     * parses the given BBCode and renders valid HTML
     *
     * @param string $bbCode the BBCode to parse and render
     * @return string the rendered BBCode
     */
    public function parse ($bbCode) {
        if (!strlen(trim($bbCode))) {
            return $bbCode;
        }
        $content = '';
        $stack = new StatusStack();
        $status = new Status();
        $part = '';
        $chars = str_split($bbCode);
        foreach ($chars as $i => $char) {
            switch ($char) {
                case '\\':
                    if ($status->string) {
                        $status->toggleEscape();
                        $part .= $char;
                        continue 2;
                    }
                    if (!$status->tag && !$status->escape && @in_array($chars[$i + 1], ['[', '\\'])) {
                        $status->escape = true;
                        continue 2;
                    }
                    break;
                case '"':
                    if ($status->tag && !$status->escape) {
                        $status->toggleString();
                    }
                    break;
                case ' ':
                case "\n":
                case "\r":
                case "\t":
                    if (!$status->tag || $status->string || $status->isEndTag()) {
                        break;
                    }
                    if (!$status->isNoTagName()) {
                        $status->attributes += $this->_parseAttribute(trim($part), $status->tagName, $i - strlen($part), $bbCode);
                    } else {
                        if ($this->_strict && !preg_match('/^[a-z_\-.\x7f-\xff][a-z0-9_\-.\x7f-\xff]*$/i', trim($part))) {
                            throw new InvalidTagNameException(h(trim($part)), $i - strlen($part), $bbCode);
                        }
                        $status->tagName = strtolower(trim($part));
                    }
                    $part = '';
                    continue 2;
                case '/':
                    if (!$status->tag || $status->string) {
                        break;
                    }
                    if (!$status->isStartTag()) {
                        if ($this->_strict) {
                            throw new UnexpectedStringException('/', $i, $bbCode);
                        } else {
                            break;
                        }
                    }
                    if (@$chars[$i - 1] == '[') {
                        if (!$stack->count()) {
                            if ($this->_strict) {
                                throw new UnexpectedStringException('END-TAG', $i, $bbCode, false);
                            } else {
                                return $status->html;
                            }
                        }
                        $content = $status->html;
                        $status = $stack->pop();
                        $status->type = TagType::EndTag;
                    } elseif (@$chars[$i + 1] == ']') {
                        $status->type = TagType::StandaloneTag;
                        $content = '';
                    } elseif ($this->_strict) {
                        throw new UnexpectedStringException('/', $i, $bbCode);
                    }
                    continue 2;
                case '[':
                    if ($status->tag && !$status->string) {
                        if ($this->_strict) {
                            throw new UnexpectedStringException('[', $i, $bbCode);
                        } else {
                            break;
                        }
                    }
                    if ($status->string || $status->escape) {
                        break;
                    }
                    $status->html .= $part;
                    $part = '';
                    $status->tag = true;
                    continue 2;
                case ']':
                    if (!$status->tag || $status->string) {
                        break;
                    }
                    if ($status->isEndTag()) {
                        if ($status->isNoTagName() && empty($status->attributes)) {
                            if ($this->_strict) {
                                throw new UnexpectedStringException('END-TAG', $i, $bbCode, false);
                            } else {
                                $part = '';
                                break;
                            }
                        }
                        while (strtolower(trim($part)) != $status->tagName) {
                            if (!$this->_isStandalone($status->tagName)) {
                                if ($this->_strict) {
                                    throw new UnexpectedStringException([h($status->tagName), h(strtolower(trim($part)))], $i - strlen($part) - 2, $bbCode, '[/%1$s]', '[/%2$s]');
                                }
                                $part = '';
                                break 2;
                            }
                            $parent = $this->_getParent($stack);
                            $content = $status->html . $this->_renderTag(new TagData($status->tagName, $status->attributes, '', $parent)) . $content;
                            if (!$stack->count()) {
                                if ($this->_strict) {
                                    throw new UnexpectedStringException('END-TAG', $i, $bbCode, false);
                                } else {
                                    return $status->html;
                                }
                            }
                            $status = $stack->pop();
                        }
                        $status->type = TagType::EndTag;
                        $part = '';
                    } else {
                        if ($status->isNoTagName()) {
                            if ($this->_strict && !preg_match('/^[a-z_\-.\x7f-\xff][a-z0-9_\-.\x7f-\xff]*$/i', trim($part))) {
                                throw new InvalidTagNameException(h(trim($part)), $i - strlen($part), $bbCode);
                            }
                            $status->tagName = strtolower(trim($part));
                            $part = '';
                        }
                        if (strlen(trim($part))) {
                            $status->attributes += $this->_parseAttribute(trim($part), $status->tagName, $i - strlen($part), $bbCode);
                        }
                        $part = '';
                    }
                    if ($status->isStartTag()) {
                        $stack->push($status);
                        $status = new Status();
                    } else {
                        $parent = $this->_getParent($stack);
                        $content = $status->html . $this->_renderTag(new TagData($status->tagName, $status->attributes, $content, $parent));
                        $status = new Status();
                        $status->html = $content;
                        $content = '';
                    }
                    continue 2;
            }
            $part .= $char;
            $status->escape = false;
        }
        if ($this->_strict && $status->tag) {
            throw new UnexpectedStringException([']', __d('BBCode', 'END_OF_STRING')], strlen($bbCode) - 1, $bbCode, "'%1\$s'", '%2$s');
        }
        while ($stack->count()) {
            $status = $stack->pop();
            if (!$this->_isStandalone($status->tagName)) {
                if ($this->_strict) {
                    throw new UnexpectedStringException([h($status->tagName), __d('BBCode', 'END_OF_STRING')], strlen($bbCode) - strlen($part), $bbCode, '[/%1$s]', '%2$s');
                }
                continue;
            }
            $parent = $this->_getParent($stack);
            $content = $status->html .= $this->_renderTag(new TagData($status->tagName, $status->attributes, '', $parent)) . $content;
        }
        return $status->html . $part;
    }

    /**
     * parses and validates a found attribute string
     *
     * @param string $attribute the attribute string to parse
     * @param string $tagName the name of the tag the attribute string belongs to
     * @param int $i the start position of the attribute string in the original BBCode
     * @param string $bbCode the original BBCode
     * @return array the parsed attribute in the form [name, value]
     * @throws AttributeFormatException in strict mode if the attribute string is invalid
     */
    private function _parseAttribute ($attribute, $tagName, $i, $bbCode) {
        if (empty($attribute)) {
            return [];
        }
        if (!preg_match('/^([a-z_\-.\x7f-\xff][a-z0-9_\-.\x7f-\xff]*)(?:="(.*)")?$/i', $attribute, $parts)) {
            if ($this->_strict) {
                throw new AttributeFormatException([h($attribute), h($tagName)], $i, $bbCode);
            } else {
                return [];
            }
        }
        return [$parts[1] => empty($parts[2]) ? $parts[1] : $parts[2]];
    }

    /**
     * renders a tag by calling and rendering the configured view cell.
     *
     * the tag name is used as action name
     *
     * @param TagData $data the tag data
     * @return string the rendered tag or an empty string if render errors occurred
     * @throws TagRenderException in strict mode if any render error occurs
     */
    private function _renderTag (TagData $data) {
        $cellName = Configure::read('BBCode.renderCell');
        $action = $data->tagName;
        if (strlen($data->content) && $this->_isStandalone($action)) {
            throw new ContentNotAllowedException(h($data->tagName));
        }
        try {
            try {
                $cell = $this->cell(sprintf('%s::%s', $cellName, $action), [$data]);
                try {
                    return $cell->render();
                } catch (BadMethodCallException $e) {
                    try {
                        $cell->action = '_' . $action;
                        return $cell->render();
                    } catch (BadMethodCallException $e2) {
                        throw $e;
                    }
                }
            } catch (MissingCellException $e) {
                throw $this->_translateException($data, $e);
            } catch (MissingCellViewException $e) {
                throw $this->_translateException($data, $e);
            } catch (BadMethodCallException $e) {
                throw $this->_translateException($data, $e);
            }
        } catch (TagRenderException $e) {
            if ($this->_strict) {
                throw $e;
            }
            return '';
        }
    }

    /**
     * translates the given exception into an UnknownTagException
     *
     * @param TagData $data the tag data
     * @param Exception $e the exception to translate
     * @return UnknownTagException the newly generated exception
     */
    private function _translateException (TagData $data, Exception $e) {
        if (Configure::read('debug')) {
            return $e;
        }
        return new UnknownTagException($data->tagName, 500, $e);
    }

    private $request;
    private $response;
    private function eventManager () {
        return EventManager::instance();
    }

    /**
     * @param string $tagName the tag name to check
     * @return bool true, if the tag is annotated as standalone, false otherwise
     */
    private function _isStandalone ($tagName) {
        $cellName = Configure::read('BBCode.renderCell');
        $className = App::className($cellName, 'View/Cell', 'Cell');
        try {
            $class = new \ReflectionClass($className);
            try {
                $method = $class->getMethod($tagName);
            } catch (\ReflectionException $e) {
                $method = $class->getMethod('_' . $tagName);
            }
        } catch (\ReflectionException $e) {
            return false;
        }
        $docComment = $method->getDocComment();
        return preg_match('/\W@standalone\W/', $docComment);
    }

    /**
     * @param StatusStack $stack the stack to get the next parent from
     * @return mixed
     */
    private function _getParent (StatusStack $stack) {
        $i = $stack->count();
        do {
            $status = $stack->get(--$i);
        } while ($this->_isStandalone($status->tagName));
        return $status->tagName;
    }

}