# CakePHP BBCode Parser Plugin

[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A CakePHP plugin for Parsing BBCode tags

## Installing via composer

You can install this plugin into your CakePHP application using
[composer](http://getcomposer.org). For existing applications you can add the
following to your `composer.json` file:

```javascript
"require": {
    "bnware/cakephp-bbcode": "*"
}
```

And run `php composer.phar update`

In your `config\bootstrap.php`:

```php
Plugin::load('BBCode', ['bootstrap' => true]);
```

## Using Internationalization & Localization

All error messages are able to be localized. You just have to use the template file in `src/Locale/BBCode.pot`.

## Customizing tag rendering

The tags are rendered by an view cell. You can customize the rendering by defining an own view cell.
The cell must contain one action for each tag. The name of the action must be the tag name with an optional preceding underscore.

You can extend the predefined tags by inheriting your view cell from the predefined one or you can define your own set of tags.
To use some rendering utils, you can use the `TagRendererTrait`.

To configure the view cell to use, add `BBCode.renderCell` to your config. The value can be a class name in plugin syntax or a full qualified class name.

## Calling the parser

The parser can be used in views by using the ```php BBCodeHelper::render($bbCode)``` method.
You just have to add the `BBCodeHelper` in your controller:

```php
$this->helpers[] = 'BBCode.BBCode';
```

You can also use the `BBCodeValidator` to check if user input is valid BBCode syntax.
You just have to define it as validation provider and define a validation rule using this provider:

```php
$validator->provider('BBCode', 'BBCode\Validation\BBCodeValidator');  
$validator->add('<field>', '<name>', ['provider' => 'BBCode', 'rule' => 'validate']);
```